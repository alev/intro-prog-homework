# LU 1 - Variables
#### LU1 - Exercise 1
stock_fav = "TSLA"
stock_value = 321.01
stock_tradevolume = 20
stock_action = "buy"
print("I would like to", stock_action,stock_tradevolume, "shares in", stock_fav)


#### LU1 - Exercise 2
person1 = "Chris"
person2 = "Cesco"
x = 10
y = 1
companyz = 'APPL'
print(person1, "needs to borrow", x , "euros from", person2, "in order to trade", y, "stocks in", companyz)


#### LU1 - Exercise 3
a = 2
b = 3
result_ex3 = a*b
print(result_ex3)


#### LU1 - Exercise 4
celsius = 30
kelvin = celsius * 9 / 5 + 32
fahrenheit = celsius + 273.15
print("degrees in celsius", celsius, "in kelvin", kelvin, "in fahrenheit", fahrenheit)


print('heyhey')